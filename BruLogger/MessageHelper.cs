﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BruLogger
{
    public static class MessageHelper
    {
        public static string Message(string format, params object[] args)
        {
            var message = string.Format(format, args);
            return message;
        }
        public static string Message(CallerContext callerContext, string format, params object[] args)
        {
            var message = string.Format(format, args) + callerContext;
            return message;
        }

        public static string MessageFormat(CallerContext callerContext, string format)
        {
            return format + callerContext;
        }
    }
}
