﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Config;

namespace BruLogger
{
    public class LogManagerLog4Net : ILogManager
    {
        private static bool _isConfigured;

        /// <summary>
        /// Create this class and configure log4net if it hasn't been done already.
        /// </summary>
        /// <param name="configFileName">null or empty means use app.config, otherwise a config file located alongside the executing assembly</param>
        public LogManagerLog4Net(string configFileName)
        {
            if (_isConfigured)
            {
                return;
            }
            if (string.IsNullOrEmpty(configFileName))
            {
                XmlConfigurator.Configure();
            }
            else
            {
                var type = typeof(LogManagerLog4Net);
                var fi = new FileInfo(type.Assembly.Location);
                var directory = fi.DirectoryName ?? "";
                var path = Path.Combine(directory, configFileName);
                if (!File.Exists(path))
                {
                    var cwd = Directory.GetCurrentDirectory(); // debug
                    var process = Process.GetCurrentProcess();
                    var appName = process.ProcessName;
                    throw new Exception($"Missing file {path} for {appName}");
                }
                XmlConfigurator.ConfigureAndWatch(new FileInfo(path));
            }
            _isConfigured = true;

        }


        /// <summary>
        /// Returns a logger associated with the specified type.
        /// (Only uses the type to get its name.)
        /// </summary>
        public ILogger ForType<T>()
        {
            var className = typeof (T).Name;
            return ForClass(className);
        }

        /// <summary>
        /// Returns a logger associated with the given className.
        /// </summary>
        /// <param name="className">Pass in null or empty for a NullLogger</param>
        public ILogger ForClass(string className)
        {
            return new LoggerLog4Net(className);
        }
    }
}
