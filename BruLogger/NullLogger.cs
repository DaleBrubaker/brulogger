﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BruLogger
{
    public class NullLogger : ILogger
    {
        public void Debug(string format, params object[] args)
        {
            // Do nothing
        }

        public void Debug(CallerContext callerContext, string format, params object[] args)
        {
            // Do nothing
        }

        public void Debug(Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Debug(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Error(string format, params object[] args)
        {
            // Do nothing
        }

        public void Error(CallerContext callerContext, string format, params object[] args)
        {
            // Do nothing
        }

        public void Error(Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Error(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Fatal(string format, params object[] args)
        {
            // Do nothing
        }

        public void Fatal(CallerContext callerContext, string format, params object[] args)
        {
            // Do nothing
        }

        public void Fatal(Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Fatal(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Info(string format, params object[] args)
        {
            // Do nothing
        }

        public void Info(CallerContext callerContext, string format, params object[] args)
        {
            // Do nothing
        }

        public void Info(Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Info(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Warn(string format, params object[] args)
        {
            // Do nothing
        }

        public void Warn(CallerContext callerContext, string format, params object[] args)
        {
            // Do nothing
        }

        public void Warn(Exception ex, string format, params object[] args)
        {
            // Do nothing
        }

        public void Warn(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            // Do nothing
        }
    }
}
