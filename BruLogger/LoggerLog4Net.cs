﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace BruLogger
{
    public class LoggerLog4Net : ILogger
    {
        private ILog _log;

        /// <summary>
        /// This is an ILogger based on log4net
        /// </summary>
        /// <param name="className">Pass in null or empty for a NullLogger</param>
        public LoggerLog4Net(string className)
        {
            if (!string.IsNullOrEmpty(className))
            {
                _log = LogManager.GetLogger(className);
            }
        }

       public void Debug(string format, params object[] args)
        {
            if (_log == null || !_log.IsDebugEnabled) return;
            _log.DebugFormat(format, args);
        }

        public void Debug(CallerContext callerContext, string format, params object[] args)
        {
            if (_log == null || !_log.IsDebugEnabled) return;
            var messageFormat = MessageHelper.MessageFormat(callerContext, format);
            _log.DebugFormat(messageFormat, args);
        }

        public void Debug(Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsDebugEnabled) return;
            var message = MessageHelper.Message(format, args);
            _log.Debug(message, ex);
        }

        public void Debug(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsDebugEnabled) return;
            var message = MessageHelper.Message(callerContext, format, args);
            _log.Debug(message, ex);
        }

        public void Error(string format, params object[] args)
        {
            if (_log == null || !_log.IsErrorEnabled) return;
            _log.ErrorFormat(format, args);
        }

        public void Error(CallerContext callerContext, string format, params object[] args)
        {
            if (_log == null || !_log.IsErrorEnabled) return;
            var messageFormat = MessageHelper.MessageFormat(callerContext, format);
            _log.ErrorFormat(messageFormat, args);
        }

        public void Error(Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsErrorEnabled) return;
            var message = MessageHelper.Message(format, args);
            _log.Error(message, ex);
        }

        public void Error(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsErrorEnabled) return;
            var message = MessageHelper.Message(callerContext, format, args);
            _log.Error(message, ex);
        }

        public void Fatal(string format, params object[] args)
        {
            if (_log == null || !_log.IsFatalEnabled) return;
            _log.FatalFormat(format, args);
        }

        public void Fatal(CallerContext callerContext, string format, params object[] args)
        {
            if (_log == null || !_log.IsFatalEnabled) return;
            var messageFormat = MessageHelper.MessageFormat(callerContext, format);
            _log.FatalFormat(messageFormat, args);
        }

        public void Fatal(Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsFatalEnabled) return;
            var message = MessageHelper.Message(format, args);
            _log.Fatal(message, ex);
        }

        public void Fatal(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsFatalEnabled) return;
            var message = MessageHelper.Message(callerContext, format, args);
            _log.Fatal(message, ex);
        }

        public void Info(string format, params object[] args)
        {
            if (_log == null || !_log.IsInfoEnabled) return;
            _log.InfoFormat(format, args);
        }

        public void Info(CallerContext callerContext, string format, params object[] args)
        {
            if (_log == null || !_log.IsInfoEnabled) return;
            var messageFormat = MessageHelper.MessageFormat(callerContext, format);
            _log.InfoFormat(messageFormat, args);
        }

        public void Info(Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsInfoEnabled) return;
            var message = MessageHelper.Message(format, args);
            _log.Info(message, ex);
        }

        public void Info(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsInfoEnabled) return;
            var message = MessageHelper.Message(callerContext, format, args);
            _log.Info(message, ex);
        }

        public void Warn(string format, params object[] args)
        {
            if (_log == null || !_log.IsWarnEnabled) return;
            _log.WarnFormat(format, args);
        }

        public void Warn(CallerContext callerContext, string format, params object[] args)
        {
            if (_log == null || !_log.IsWarnEnabled) return;
            var messageFormat = MessageHelper.MessageFormat(callerContext, format);
            _log.WarnFormat(messageFormat, args);
        }

        public void Warn(Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsWarnEnabled) return;
            var message = MessageHelper.Message(format, args);
            _log.Warn(message, ex);
        }

        public void Warn(CallerContext callerContext, Exception ex, string format, params object[] args)
        {
            if (_log == null || !_log.IsWarnEnabled) return;
            var message = MessageHelper.Message(callerContext, format, args);
            _log.Warn(message, ex);
        }
    }
}
