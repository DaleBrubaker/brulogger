﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BruLogger
{
    public interface ILogManager
    {
        /// <summary>
        /// Returns a logger associated with the specified type.
        /// (Only uses the type to get its name.)
        /// </summary>
        ILogger ForType<T>();

        /// <summary>
        /// Returns a logger associated with the given className.
        /// </summary>
        /// <param name="className">Pass in null or empty for a NullLogger</param>
        ILogger ForClass(string className);
    }
}
