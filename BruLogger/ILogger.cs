﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BruLogger
{
    public interface ILogger
    {
        void Debug(string format, params object[] args);
        void Debug(CallerContext callerContext, string format, params object[] args);
        void Debug(Exception ex, string format, params object[] args);
        void Debug(CallerContext callerContext, Exception ex, string format, params object[] args);
        void Error(string format, params object[] args);
        void Error(CallerContext callerContext, string format, params object[] args);
        void Error(Exception ex, string format, params object[] args);
        void Error(CallerContext callerContext, Exception ex, string format, params object[] args);
        void Fatal(string format, params object[] args);
        void Fatal(CallerContext callerContext, string format, params object[] args);
        void Fatal(Exception ex, string format, params object[] args);
        void Fatal(CallerContext callerContext, Exception ex, string format, params object[] args);
        void Info(string format, params object[] args);
        void Info(CallerContext callerContext, string format, params object[] args);
        void Info(Exception ex, string format, params object[] args);
        void Info(CallerContext callerContext, Exception ex, string format, params object[] args);
        void Warn(string format, params object[] args);
        void Warn(CallerContext callerContext, string format, params object[] args);
        void Warn(Exception ex, string format, params object[] args);
        void Warn(CallerContext callerContext, Exception ex, string format, params object[] args);
    }
}
