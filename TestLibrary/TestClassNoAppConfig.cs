﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using BruLogger;

namespace TestLibrary
{
    public class TestClassNoAppConfig
    {
        /// <summary>
        /// We use the log4net.config file, whose properties are set to "copy if newer"
        /// </summary>
        private static readonly ILogger Log = new LogManagerLog4Net("log4net.config").ForType<TestClass>();

        public TestClassNoAppConfig()
        {
            Log.Debug(new CallerContext(), "Here we are! {0}", "test");
        }
    }
}
