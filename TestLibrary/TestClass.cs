﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using BruLogger;

namespace TestLibrary
{
    public class TestClass
    {
        /// <summary>
        /// We use the App.config file, whose properties are set to "copy if newer"
        /// </summary>

        private static readonly ILogger Log = new LogManagerLog4Net(null).ForType<TestClass>();

        public TestClass()
        {
            Log.Debug(new CallerContext(), "Here we are! {0}", "test");
        }
    }
}
