BruLogger
=========

Dale Brubaker

March, 2016

The BruLogger library is a simple logger interface that shows how to “hide” a dependency on a particular logger. The first implementation is for log4net.

Features:
---------

1.  Hide the logger implementation in a simple and flexible manner.

2.  Append caller information to the end of the message:

    1.  CallerMemberName

    2.  CallerFilePath

    3.  CallerLineNumber

BruLogger is the base library that has a dependency on log4net.

TestLibrary depends on BruLogger.

TestConsoleApplication depends on TestLibrary but doesn’t depend on either BruLogger or log4net..

NuGet is used to restore the log4net dependency when the solution is built.
